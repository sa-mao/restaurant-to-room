var config = {};

config.mongoUri = 'mongodb://localhost:27017/rtr';
config.cookieMaxAge = 30 * 24 * 3600 * 1000 ; 
config.ordrxKey = 'eHi_i_bl46a-c17bhatpfrlkjo5vsP6hm9l2swyZNMM';

config.address = {
    addr: '288 Coleridge St',
    city: 'San Francisco',
    zip: '94110',
    state: 'CA'
};
config.phone = '415-555-1234';

module.exports = config;