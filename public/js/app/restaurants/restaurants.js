(function() {
    'use strict';

    var app = angular.module('app');
    
    app.controller('RestaurantsController', RestaurantsController);
    
    RestaurantsController.$inject = ['api'];
    
    function RestaurantsController(api){
        var vm = this; 
        
        api.getRestaurants()
            .then(function(data){
                vm.restaurants = data;
            });
    };
})();